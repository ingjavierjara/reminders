﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reminders.Controllers
{
    public class RemindersController : Controller
    {
        //
        // GET: /Reminders/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Reminders/Details/5

        public ActionResult Details(int id)
        {
            var rm = new Reminders.Models.RemindersModel();
            var r = rm.GetReminder(id);//.ToArray();
            var result = Json(r, JsonRequestBehavior.AllowGet);

            return result;
        }

        //
        // POST: /Reminders/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            bool res = false;
            string _err = "";
            try
            {
                if (collection["dataReminder"] != null)
                {
                    Reminders.Models.RemindersModel rm = new Reminders.Models.RemindersModel();
                    Reminders.Models.Reminders r = new Reminders.Models.Reminders();
                    r.idUser = 1;
                    r.idReminder = 0 ;
                    r.Reminder = collection["dataReminder"];
                    r._DateCreate = DateTime.Now;
                    System.IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-US", true);
                    if(collection["dataDateLimit"]!="") r._DateLimit = DateTime.ParseExact(collection["dataDateLimit"], "MM-dd-yyyy", dateFormat); 
                    res = rm.Update(r);
                    if (!res) _err = rm._lastError;
                }
                else {
                    _err = "Empty Reminder";
                }
            }
            catch(Exception ex)
            {
                _err = ex.Message;
            }
            return Json(new { success = res, message = _err });
        }
        
        //
        // POST: /Reminders/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            bool res = false;
            string _err = "";
            try
            {
                if (collection["dataReminder"] != null)
                {
                    Reminders.Models.RemindersModel rm = new Reminders.Models.RemindersModel();
                    Reminders.Models.Reminders r = new Reminders.Models.Reminders();
                    r.idUser = 1;
                    r.idReminder = id;
                    r.Reminder = collection["dataReminder"];
                    r._DateCreate = DateTime.Now;
                    System.IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-US", true);
                    if (collection["dataDateLimit"] != "") r._DateLimit = DateTime.ParseExact(collection["dataDateLimit"], "MM-dd-yyyy", dateFormat);
                    res = rm.Update(r);
                    if (!res) _err = rm._lastError;
                }
                else
                {
                    _err = "Empty Reminder";
                }
            }
            catch (Exception ex)
            {
                _err = ex.Message;
            }
            return Json(new { success = res, message = _err });
        }
        //
        // POST: /Reminders/Complete/5

        [HttpPost]
        public ActionResult Complete(int id, FormCollection collection)
        {
            bool res = false;
            string _err = "";
            try
            {
                if (collection["dataidReminder"] != null && collection["dataidReminder"] == id.ToString())
                {
                    Reminders.Models.RemindersModel rm = new Reminders.Models.RemindersModel();
                    res = rm.Complete(id);
                    if (!res) _err = rm._lastError;
                }
                else
                {
                    _err = "Invalid idReminder";
                }
            }
            catch (Exception ex)
            {
                _err = ex.Message;
            }
            return Json(new { success = res, message = _err });
        }
        //
        // POST: /Reminders/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            bool res = false;
            string _err = "";
            try
            {
                if (collection["dataidReminder"] != null && collection["dataidReminder"]==id.ToString())
                {
                    Reminders.Models.RemindersModel rm = new Reminders.Models.RemindersModel();
                    res = rm.Delete(id);
                    if (!res) _err = rm._lastError;
                }
                else
                {
                    _err = "Invalid idReminder";
                }
               
            }
            catch(Exception ex)
            {
                _err = ex.Message;
            }
            return Json(new { success = res, message = _err });
        }


        //
        // GET: /Reminders/

        public ActionResult GetReminders()
        {
            var rm=new Reminders.Models.RemindersModel();
            var r = rm.GetReminders();//.ToArray();
            var result = Json(r, JsonRequestBehavior.AllowGet);
          
            return result;
        }
    }
}
