﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reminders.Models
{
    public class RemindersModel
    {
        public string _lastError;
        RemindersDBDataContext ctx;
        public RemindersModel()
        {
            ctx = new RemindersDBDataContext();
        }

        public IEnumerable<Reminders> GetReminders()
        {
            List<Reminders> Result=new List<Reminders>();
            try
            {
                    Result = ctx.Reminders.ToList();
            }
            catch (Exception ex)
            {
                _lastError = ex.Message;
            }
            return Result;
        }
        public IEnumerable<Reminders> GetReminder(int idReminder)
        {
            List<Reminders> Result = new List<Reminders>();
            try
            {
                var rows = (from r in ctx.Reminders
                                  where r.idReminder == idReminder
                                  select r);
                Result = rows.ToList();
            }
            catch (Exception ex)
            {
                _lastError = ex.Message;
            }
            return Result;
        }

        public bool Update(Reminders data)
        {
            bool res = false;
            try
            {
                using (var ctx = new RemindersDBDataContext())
                {
                    if (data.idReminder == 0)
                    {
                        ctx.Reminders.InsertOnSubmit(data);
                    }
                    else {
                        var row = ctx.Reminders.Single(R => R.idReminder == data.idReminder);
                        row.Reminder = data.Reminder;
                        row._DateLimit=data._DateLimit;
                    }
                    ctx.SubmitChanges();
                }
                res = true;
            }
            catch (Exception ex)
            {
                _lastError = ex.Message;
            }
            return res;
        }

        public bool Complete(int idReminder)
        {
            bool res = false;
            try
            {
                using (var ctx = new RemindersDBDataContext())
                {
                    var row = ctx.Reminders.Single(R => R.idReminder == idReminder);
                    if (row != null) row._DateCompleted=DateTime.Now;
                    ctx.SubmitChanges();
                }
                res = true;
            }
            catch (Exception ex)
            {
                _lastError = ex.Message;
            }
            return res;
        }

        public bool Delete(int idReminder)
        {
            bool res = false;
            try
            {
                using (var ctx = new RemindersDBDataContext())
                {
                    var row = ctx.Reminders.Single(R => R.idReminder == idReminder);
                    if(row!=null) ctx.Reminders.DeleteOnSubmit(row);
                    ctx.SubmitChanges();
                }
                res = true;
            }
            catch (Exception ex)
            {
                _lastError = ex.Message;
            }
            return res;
        }
    }
}