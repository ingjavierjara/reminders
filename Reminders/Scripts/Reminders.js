﻿//Protype return date formated MM-dd-yyyy
 Date.prototype.toMMDDYYYY = function () {return isNaN (this) ? 'NaN' : [this.getMonth() > 8 ? this.getMonth() + 1 : '0' + (this.getMonth() + 1), this.getDate() > 9 ? this.getDate() : '0' + this.getDate(), this.getFullYear()].join('-')}
       
        //Function change carrier returns
        function return2br(dataStr) {
            return dataStr.replace(/(\r\n|[\r\n])/g, "<br />");
        }
        //Validate Data Required Form.
        function validate()
        {
            $("#dataReminder").val($.trim($("#dataReminder").val()));
            if($("#dataReminder").val()=="")
            {
                alert("Empty Reminder");
                return false;
            }
            return true;
        }

        //clear forms data      
        function clearForm()
        {
            $('#chkLimit').attr('checked', false);
            $('#dataDateLimit').hide();
            $('#dataDateLimit').val('');
            $('#dataReminder').val('');
            $('#dataidReminder').val(0);
            $('#txtTitleForm').text('New Reminder');
            $('#btnDelete').hide();
        }
        //show icons command on mouseover event
        function showicon(index)
        {
            $("#i" + index).show();
            $("#ic" + index).show();
        }
        function hideicon(index)
        {
            $("#i" + index).hide();
            $("#ic" + index).hide();
        }


        $(document).ready(function () {
            $.ajaxSetup({ cache: false });
            //Setup Control datepicker
            $('#dataDateLimit').val(datedefault);
            $('#dataDateLimit').datepicker({ format: 'mm-dd-yyyy' });
            //add handler event click
            $('#btnNew').on('click', function (ev) {
                clearForm();
                $('#Form').modal();
            });
            $('#btnSave').on('click', function (ev) {
                if (validate()) {
                    save();
                }
            });
            $('#btnDelete').on('click', function (ev) {
                Delete();
            });
            $('#chkLimit').on('click', function (ev) {
                if ($('#chkLimit').is(":checked") == false) {
                    $('#dataDateLimit').val("");
                    $('#dataDateLimit').hide();
                }
                else {
                    $('#dataDateLimit').show();
                }
            });
            loadReminders();
        });

        //Load via ajax list reminders data
        function loadReminders() {

            $.getJSON(URL_GetReminders,
            function (data) {
                $("#list").empty();
                $("#list").append("<ul class='nav nav-pills nav-stacked'>");
                $("#listCompleted").empty();
                $("#listCompleted").append("<ul class='nav nav-pills nav-stacked'>");

                var list = "<ul class='nav nav-pills nav-stacked'>";
                var listCompleted = "<ul class='nav nav-pills nav-stacked'>";
                $.each(data, function (i, item) {
                    if (item._DateCompleted == null) {
                        var color = "";
                        var dl = "";
                        if (item._DateLimit) {
                            var _dl = new Date(parseInt(item._DateLimit.substr(6)))
                            dl = "<br /><span class='btn-small' style='color: gray;'>Limit:" + _dl + "</span>";
                            if (_dl <= new Date) color = " style='color: red;'"
                        }
                        list += "<li><a href='#' onmouseover='showicon(" + i + ");' " + color + " onmouseout='hideicon(" + i + ");'>" + return2br(item.Reminder);
                        list += "<i id='i" + i + "' class='icon-pencil' style='float:right;display:none;' onclick='edit(" + i + "," + item.idReminder + ");' />";
                        list += "<i id='ic" + i + "' class='icon-ok' style='float:right;display:none;' onclick='complete(" + i + "," + item.idReminder + ");' />";
                        list += dl+"</a></li>";
                    } else {
                        var dc = new Date(parseInt(item._DateCompleted.substr(6)))
                        listCompleted += "<li><a href='#' onmouseover='showicon(" + i + ");'  onmouseout='hideicon(" + i + ");'>" + return2br(item.Reminder) + "<br />";
                        listCompleted += "<i id='i" + i + "' class='icon-pencil' style='float:right;display:none;' onclick='edit(" + i + "," + item.idReminder + ");' />";
                        listCompleted += "<i id='ic" + i + "' class='icon-trash' style='float:right;display:none;' onclick='Delete(" + i + "," + item.idReminder + ");' />";
                        listCompleted += "<span class='btn-small' style='color: gray;'>Completed:" + dc + "</span></a></li>";
                    }
                });
                $("#list").append(list + "</ul>");
                $("#listCompleted").append(listCompleted + "</ul>");

            });
        }

        // function call add or edit action.
        function save() {
            var myData = $('#MyForm').serialize();
            var urlaction = URL_Create;
            if ($('#dataidReminder').val() != "") urlaction = URL_Edit + "/" + $('#dataidReminder').val();
            if (confirm("Sure you want to save the info ?")) {
                $.post(urlaction, myData, function (data) {
                    if (data.success == true) { $('#Form').modal('hide'); loadReminders(); }
                    else { alert(data.message); }
                });
            }
        }
        //call delete action and reload list
        function Delete(index, id) {
            if (confirm("Sure you want to delete ?")) {
                if (id) $('#dataidReminder').val(id);
                var myData = $('#MyForm').serialize();
                $.post(URL_Delete + "/" + $('#dataidReminder').val(), myData, function (data) {
                    if (data.success == true) {
                        alert("Reminder Deleted!");
                        $('#Form').modal('hide');
                        loadReminders();
                    }
                    else { alert(data.message); }
                });
            }
        }
        //call details action, load info and show modal form
        function edit(index, id) {
            
                clearForm();
                $.post(URL_Details + "/" + id, null, function (data) {
                    if (data) {
                        $('#dataidReminder').val(data[0].idReminder);
                        $('#dataReminder').val(data[0].Reminder);
                        if (data[0]._DateLimit) {
                            var dl = new Date(parseInt(data[0]._DateLimit.substr(6)));
                            $('#dataDateLimit').val(dl.toMMDDYYYY());
                            $('#dataDateLimit').show();
                            $('#chkLimit').attr('checked', true);
                        }
                        $('#txtTitleForm').text('Edit Reminder');
                        $('#btnDelete').show();

                        $('#Form').modal();
                    }
                    else {
                        alert("Error trying load data");
                    }
                });
            
        }

        //call complete action, mark completed the reminder and reload list
        function complete(index, id) {
            if (confirm("Sure you want to complete ?")) {
                clearForm();
                $('#dataidReminder').val(id);
                var myData = $('#MyForm').serialize();
                $.post(URL_Complete + "/" + id, myData, function (data) {
                    if (data.success == true) {
                        alert("Reminder Completed!");
                        loadReminders();
                    }
                    else { alert(data.message); }
                });
            }
        }