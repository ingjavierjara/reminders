﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    My List
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpScripts" runat="server">
   
    <script>
    var datedefault='<%= DateTime.Now.ToString("MM-dd-yyyy") %>';
    var URL_GetReminders="<%= Url.Action("GetReminders","Reminders") %>";
    var URL_Create="<%= Url.Action("Create","Reminders") %>";
    var URL_Edit="<%= Url.Action("Edit","Reminders") %>";
    var URL_Delete="<%= Url.Action("Delete","Reminders") %>";
    var URL_Details="<%= Url.Action("Details","Reminders") %>"; 
    var URL_Complete="<%= Url.Action("Complete","Reminders") %>";   
    </script> 
       <script src="../../Scripts/Reminders.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent"  runat="server">
    <div class="row-fluid">
        <div class="well sidebar-nav">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#listPendings" data-toggle="tab">Pendings</a></li>
            <li><a href="#listCompleted"  data-toggle="tab">Completed</a></li>
        </ul>
        <div class="tab-content">
            <div id="listPendings" class="tab-pane active">
                <div id="list" class="tab-pane active"></div>
                <a class="btn btn-small" href="#" id="btnNew"><i class="icon-plus"></i>New</a>
            </div>
            <div id="listCompleted" class="tab-pane"></div>
        </div>

        <div class="modal hide" style="width:320px;" id="Form">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h3 id="txtTitleForm">New Remind</h3>
            </div>
            <div class="modal-body">
                <div style="width:300px;">
                    <form id="MyForm">
                    <div>
                         <label for="txtRemind">Remind</label>
                         <div class="controls">
                            <textarea id="dataReminder" name="dataReminder" class="input-xlarge" rows="3"></textarea>
                        </div>
                    </div>
                    <div>
                        <div class="controls">
                            <label class="checkbox">
                            <input id="chkLimit" type="checkbox" value="1" />
                            Limit Date
                            </label>
                        </div>                    
                    </div>
                    <div>
                            <input type="text" style="width:100px;display:none;" name="dataDateLimit" class="span2" value="" id="dataDateLimit" />
                            <input id="dataidReminder" name="dataidReminder" type="hidden" value="1" />
                     </div>
                     </form>
                 </div>
            </div>
            <div class="modal-footer">
            <a href="#" class="icon-trash" style="float:left;" id="btnDelete"></a>
            <a href="#" class="btn" data-dismiss="modal">Close</a>
            <a href="#" class="btn btn-primary" id="btnSave">Save changes</a>
            </div>
        </div>        
        </div>
    </div>
</asp:Content>
